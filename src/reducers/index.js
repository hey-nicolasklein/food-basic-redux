import counterReducer from './counter';
import isLoggedReducer from './isLogged';
import {combineReducers} from 'redux';


const masterReducer = combineReducers({
  counter: counterReducer,
  logged: isLoggedReducer
})


export default masterReducer;