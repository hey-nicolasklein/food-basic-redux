import React from 'react';
import './App.css';
import {useSelector, useDispatch} from 'react-redux';
import {increment, decrement} from './actions';

function App() {

  const counter = useSelector(state => state.counter);
  const isLogged = useSelector(state => state.isLogged);
  const dispatch = useDispatch();

  const boundIncrement = number => dispatch(increment(number));

  return (
    <div className="App">
      <h1>Counter = {counter}</h1>
      <button onClick={() => boundIncrement(5)}>+1</button>
      <button onClick={() => dispatch(decrement())}>-1</button>
      {isLogged ? <h3>Secret</h3> : ''}
    </div>
  );
}

export default App;
